SSAKE wrapper
==============

Version history
---------------

- Unreleased: Update Orione citation.
- Release 2: Fix SSAKE download URL (thanks to Graham Etherington for reporting this). Update Orione citation.
- Release 1: Update to SSAKE 3.8.1. Add <version_command>. Make pylint happier. Simplify code. Add readme.rst .
- Release 0: Initial release in the Tool Shed.

Development
-----------

Development is hosted at https://bitbucket.org/crs4/orione-tools . Contributions and bug reports are very welcome!

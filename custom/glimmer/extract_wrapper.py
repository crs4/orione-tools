# -*- coding: utf-8 -*-
"""
Glimmer --> extract
version 0.2 (andrea.pinna@crs4.it)
"""

import optparse
import subprocess
import sys

def __main__():
    # load arguments
    print 'Parsing Extract input options...'
    parser = optparse.OptionParser()
    parser.add_option('--exSequence', dest='sequence', help='')
    parser.add_option('--exCoords', dest='coords', help='')
    parser.add_option('--ex2Fields', action='store_true', dest='twofields', help='')
    parser.add_option('--exDir', action='store_true', dest='dir', help='')
    parser.add_option('--exMinLen', dest='minlen', type='int', help='')
    parser.add_option('--exNoStart', action='store_true', dest='nostart', help='')
    parser.add_option('--exNoStop', action='store_true', dest='nostop', help='')
    parser.add_option('--exNoWrap', action='store_true', dest='nowrap', help='')
    parser.add_option('--exOutput', dest='output', help='')
    parser.add_option('--logfile', dest='logfile', help='')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')

    # build Extract command to be executed
    sequence = options.sequence
    coords = options.coords
    twofields = '--2_fields' if options.twofields else ''
    direct = '--dir' if options.dir else ''
    minlen = '--minlen %d' % (options.minlen) if options.minlen is not None else ''
    nostart = '--nostart' if options.nostart else ''
    nostop = '--nostop' if options.nostop else ''
    nowrap = '--nowrap' if options.nowrap else ''
    output = options.output
    logfile = options.logfile

    # Build Extract command
    cmd = 'extract %s %s %s %s %s %s %s %s > %s' % (twofields, direct, minlen, nostart, nostop, nowrap, sequence, coords, output)
    print '\nExtract command to be executed:\n %s' % (cmd)

    print 'Executing Extract...'
    log = open(logfile, 'w') if logfile else sys.stdout
    try:
        subprocess.check_call(cmd, stdout=log, stderr=subprocess.STDOUT, shell=True)
    finally:
        if log != sys.stdout:
            log.close()
    print 'Extract executed!'


if __name__ == "__main__":
    __main__()

"""
Output Format
Column 1    The chromosome name
Column 2    The genomic coordinate started from '1'
Column 3    The variant category, homozygous or heterozygous
Column 4    The reference allele
Column 5    The called allele
Column 6    The genotype posterior probability
Column 7    The variant quality score
Column 8    The median mapping quality within a window
Column 9    The median base quality within a window

PyroHMMvar is a tool to detect the SNPs and short INDELs for both Ion Torrent and 454 sequencing data.
USAGE:
pyrohmmvar -b <bam>|<sam> -f <ref.fa> -w <width> -m <hmm> -t <thresh>

 -b             The alignment file which is BAM format and also indexed by Samtools
 -f             The reference sequence which is FASTA format and also indexed by Samtools
 -r             Region specification [NULL]
 -w    INT      The size of the scanning window [20]
 -m             The parameter setting of PyroHMM
 -s    FLOAT    The prior of snp [0]
 -i    FLOAT    The prior of indel [0]
 -B    INT      The minimal base quality [20]
 -M    INT      The minimal map quality [10]
 -t    FLOAT    The minimal variant quality score [50.0]
 -H    INT      The number of paths allowed within a window [5]
 -L    INT      The minimal read length [100]
 -I             Ignore INDELs
 -c    INT      The maximum number of coverage [100]
 -E    FLOAT    The minimal alignment identity [0.9]
 -D    INT      The minimal number of reads supporting the difference [2]
 -N    INT      The minimal number of reads supporting the indels [5]
 -e    FLOAT    The coefficient e is used in the function D+e*(coverage-10)
                to compute the minimal number of reads supporting the variant [0.2]
"""
import argparse
import os


class PyrHMMvar:

    def __init__(self, alnfile, referencefile, outfile):#, logfile):
        self.alnfile = alnfile
        self.referencefile = referencefile
        self.window = 20
        self.thresh = 50.0
        self.outfile = outfile
        self.logfile = 'pyrohmmvar.log'
        self.wd = self.alnfile.rsplit("/", 1)[0]

    def run(self):
        """ """
        self._prepareCalibrationFile()
        self._indexingFiles()
        self._run()
        self._cleanEnvironment()

    def _prepareCalibrationFile(self):
        """ """
        out = open(self.wd+'calibfile.cal', 'w')
        out.write("<#seg>\n1\n</#seg>\n<transite>\n<segment> 0 \n")
        out.write("<b> 0.146424 0.852599 0.000193386 0.000193441 0.000590153 0 0 \n")
        out.write("<h> 0.00116289 0.995162 0.00116335 0.0011629 0.0013491 0 0 \n")
        out.write("<c> 0 0.975079 0.00193228 0.000807655 0.00788852 0.00213494 0.0121574 \n")
        out.write("<m> 0 0.910757 0.0596861 0.0021728 0.0251338 0.00112598 0.00112446 \n")
        out.write("<d> 0 0.94979 0.00504593 0.0332233 0.00703646 0.00245397 0.00244995 \n")
        out.write("<n> 0 0.873042 0.0146213 0.00167226 0.109664 0.000357173 0.000643928 \n")
        out.write("<y> 0 0 0 0 0 0.00133204 0.998668 \n")
        out.write("</segment>\n</transite>\n<substitute>\n<segment> 0 \n")
        out.write("<a> 0.995401 0.00126291 0.00197603 0.00136016 \n")
        out.write("<c> 0.0016503 0.994365 0.0017684 0.0022167 \n")
        out.write("<g> 0.00237126 0.00164686 0.994497 0.00148457 \n")
        out.write("<t> 0.00138192 0.00189824 0.00123809 0.995482 \n")
        out.write("<-> 0.30162 0.209336 0.203581 0.285463 \n")
        out.write("</segment>\n</substitute>\n<alpha>\n<segment> 0 \n")
        out.write("0.143683 0.153364 0.154701 0.157309 \n")
        out.write("</segment>\n</alpha>\n<beta>\n<segment> 0 \n")
        out.write("0.0451873 0.014108 1.47536 0.0475905 0.0104009 1.41055 0.0447538 0.013486 1.46909 0.046531 0.0126282 1.44322 \n")
        out.write("</segment>\n</beta>\n")
        out.close()

    def _run(self):
        """ """
        command = "pyrohmmvar -b %s.sorted.bam -f %s -w %d -m %s -t %s > %s 2>%s" % (self.alnfile, self.referencefile, self.window, self.wd+'calibfile.cal', self.thresh, self.outfile, self.wd+self.logfile)
        os.popen(command)

    def _indexingFiles(self):
        """ """
        #print "indexing..."
        command = "samtools faidx %s" % (self.referencefile)
        os.popen(command)
        command = "samtools sort %s %s" % (self.alnfile, self.alnfile+'.sorted')
        os.popen(command)
        command = "samtools index %s.bam" % (self.alnfile+'.sorted')
        os.popen(command)

    def _cleanEnvironment(self):
        """ """
        os.remove(self.wd+'calibfile.cal')
        os.remove(self.wd+self.logfile)
        os.remove(self.alnfile+'.sorted.bam')
        os.remove(self.alnfile+'.sorted.bam.bai')
        os.remove(self.referencefile+'.fai')


def __main__():
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', dest='alnfile', action='store', required=True, help='Draft File Mandatory ')
    parser.add_argument('-r', dest='referencefile', action='store', required=True, help='Contigs File Mandatory ')
    parser.add_argument('-o', dest='outfile', action='store', required=False, default="N", help='Vcf File Mandatory ')
    #parser.add_argument('-l', dest='logfile', action='store', required=False, default="2", help='Vcf File Mandatory ')
    args = parser.parse_args()

    P = PyrHMMvar(args.alnfile, args.referencefile, args.outfile)#, args.logfile)
    P.run()


if __name__ == "__main__":
    __main__()

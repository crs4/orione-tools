<tool id="pyrohmmvar" name="pyroHMMvar" version="1.0.1">
  <description></description>
  <command interpreter="python">runPyroVar.py -a $alignfile -r $reference -o $outfile
  </command>
  <inputs>
    <param name="alignfile"  type="data" format="bam"   label="Alignment File" />
    <param name="reference"  type="data" format="fasta" label="Reference File" />
  </inputs>
  <outputs>
    <data name="outfile" format="txt" label="pyroHMMvar outfile" />
  </outputs>
  <help>
**What it does**

PyroHMMvar is a tool to detect the SNPs and short INDELs for both Ion Torrent and 454 sequencing data.

USAGE:
pyrohmmvar -b &lt;bam|sam&gt; -f &lt;ref.fa&gt; -w &lt;width&gt; -m &lt;hmm&gt; -t &lt;thresh&gt;
 
 -b             The alignment file which is BAM format and also indexed by Samtools
 -f             The reference sequence which is FASTA format and also indexed by Samtools
 -r             Region specification [NULL]
 -w    INT      The size of the scanning window [20]
 -m             The parameter setting of PyroHMM
 -s    FLOAT    The prior of snp [0]
 -i    FLOAT    The prior of indel [0]
 -B    INT      The minimal base quality [20]
 -M    INT      The minimal map quality [10]
 -t    FLOAT    The minimal variant quality score [50.0]
 -H    INT      The number of paths allowed within a window [5]
 -L    INT      The minimal read length [100]
 -I             Ignore INDELs
 -c    INT      The maximum number of coverage [100]
 -E    FLOAT    The minimal alignment identity [0.9]
 -D    INT      The minimal number of reads supporting the difference [2]
 -N    INT      The minimal number of reads supporting the indels [5]
 -e    FLOAT    The coefficient e is used in the function D+e*(coverage-10)
                to compute the minimal number of reads supporting the variant [0.2]

**License and citation**

This Galaxy tool is Copyright © 2014 `CRS4 Srl.`_ and is released under the `MIT license`_.

.. _CRS4 Srl.: http://www.crs4.it/
.. _MIT license: http://opensource.org/licenses/MIT

You can use this tool only if you agree to the license terms of: `PyroHMMvar`_.

.. _PyroHMMvar: https://code.google.com/p/pyrohmmvar/

If you use this tool, please cite:

- |Cuccuru2014|_
- |Zeng2013|_.

.. |Cuccuru2014| replace:: Cuccuru, G., Orsini, M., Pinna, A., Sbardellati, A., Soranzo, N., Travaglione, A., Uva, P., Zanetti, G., Fotia, G. (2014) Orione, a web-based framework for NGS analysis in microbiology. *Bioinformatics* 30(13), 1928-1929
.. _Cuccuru2014: http://bioinformatics.oxfordjournals.org/content/30/13/1928
.. |Zeng2013| replace:: Zeng, F., Jiang, R., Chen, T. (2013) PyroHMMvar: a sensitive and accurate method to call short indels and SNPs for Ion Torrent and 454 data. *Bioinformatics* 29(22), 2859-2868
.. _Zeng2013: http://bioinformatics.oxfordjournals.org/content/29/22/2859
  </help>
</tool>

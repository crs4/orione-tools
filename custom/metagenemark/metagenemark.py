# -*- coding: utf-8 -*-
"""
MetaGeneMark
version 0.1 (andrea.pinna@crs4.it)
"""

import optparse
import shutil
import subprocess
import sys

def __main__():
    ex_path = 'gmhmmp'
    nt_path = 'nt_from_gff.pl'
    aa_path = 'aa_from_gff.pl'

    # load arguments
    print 'Parsing MetaGeneMark input options...'
    parser = optparse.OptionParser()
    parser.add_option('--input_file', dest='input_file')
    parser.add_option('--gfp_file', dest='gfp_file')
    parser.add_option('--output_format', dest='output_format')
    parser.add_option('--nucleotide_sequence', action='store_true', dest='nucleotide_sequence')
    parser.add_option('--aminoacid_sequence', action='store_true', dest='aminoacid_sequence')
    parser.add_option('--show_protein', action='store_true', dest='show_protein')
    parser.add_option('--show_nucleotide', action='store_true', dest='show_nucleotide')
    parser.add_option('--rbs_location_score', action='store_true', dest='rbs_location_score')
    parser.add_option('--rbs_scores_spacer', action='store_true', dest='rbs_scores_spacer')
    parser.add_option('--use_rbs', action='store_true', dest='use_rbs')
    parser.add_option('--strand', dest='strand')
    parser.add_option('--gene_overlap', dest='gene_overlap')
    parser.add_option('--external_information_file', dest='external_information_file')
    parser.add_option('--turn_off', dest='turn_off')
    parser.add_option('--probability', dest='probability')
    parser.add_option('--output_lst', dest='output_lst')
    parser.add_option('--output_gff', dest='output_gff')
    parser.add_option('--output_nt', dest='output_nt')
    parser.add_option('--output_aa', dest='output_aa')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')

    # build MetaGeneMark command to be executed
    input_file = options.input_file
    gfp_file = "-m %s" % options.gfp_file
    if options.output_format == 'lst':
        output_format = '-f L'
    elif options.output_format == 'gff':
        output_format = '-f G'
    show_protein = '-a' if options.show_protein is not None else ''
    show_nucleotide = '-d' if options.show_nucleotide is not None else ''
    rbs_location_score = '-K' if options.rbs_location_score is not None else ''
    rbs_scores_spacer = '-k' if options.rbs_scores_spacer is not None else ''
    use_rbs = '-r' if options.use_rbs is not None else ''
    if options.strand == 'both':
        strand = '-s .'
    elif options.strand == 'r':
        strand = '-s r'
    elif options.strand == 'd':
        strand = '-s d'
    if options.gene_overlap == 'one':
        gene_overlap = '-p 1'
    elif options.gene_overlap == 'zero':
        gene_overlap = '-p 0'
    external_information_file = '-e %s' % (options.external_information_file) if options.external_information_file is not None else ''
    turn_off = '-n' if options.turn_off is not None else ''
    probability = '-i %s' % (options.probability) if options.probability is not None else ''
    output_file = 'metagenemark'
    output = '-o %s' % output_file
    # output file(s)
    logfile = 'log'

    # Build MetaGeneMark command
    cmd = '%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s' % (ex_path, use_rbs, gfp_file, output, show_protein, show_nucleotide, output_format, rbs_location_score, rbs_scores_spacer, use_rbs, strand, gene_overlap, external_information_file, turn_off, probability, input_file)
    print '\nMetaGeneMark command to be executed:\n %s' % (cmd)

    # Execution of MetaGeneMark
    print 'Executing MetaGeneMark...'
    log = open(logfile, 'w') if logfile else sys.stdout
    try:
        subprocess.check_call(cmd, stdout=log, stderr=subprocess.STDOUT, shell=True) # need to redirect stderr because MetaGeneMark writes some logging info there
        if options.output_format == 'lst':
            shutil.move(output_file, options.output_lst)
        if options.output_format == 'gff':
            shutil.move(output_file, options.output_gff)
            if options.nucleotide_sequence:
                cmd_nt = '%s < %s > %s' % (nt_path, options.output_gff, options.output_nt)
                print '\nNucleotide sequence parser command to be executed:\n %s' % (cmd_nt)
                subprocess.check_call(cmd_nt, shell=True)
            if options.aminoacid_sequence:
                cmd_aa = '%s < %s > %s' % (aa_path, options.output_gff, options.output_aa)
                print '\nAmino acid sequence parser command to be executed:\n %s' % (cmd_aa)
                subprocess.check_call(cmd_aa, shell=True)
    finally:
        if log != sys.stdout:
            log.close()
    print 'MetaGeneMark executed!'
    

if __name__ == "__main__":
    __main__()
